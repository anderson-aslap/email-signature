> Entrar no projeto e executar o comando abaixo

```bash
$ php -S localhost:8000
```

> Acessar a seguinte URL

```bash
http://localhost:8000/
```
