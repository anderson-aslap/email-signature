<?php

require_once 'init.php';

$dataPeople = getPeople();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Assinatura E-mail</title>

    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:400,700"
    />

    <style>
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
      }

      body {
        font-family: 'Roboto', sans-serif;
      }

      ul {
        padding: 40px 80px;
      }

      li {
        padding: 10px 0;
      }

      a {
        text-decoration: none;
        font-weight: bold;
      }

      
    </style>
  </head>
  <body>
    <ul>
      <?php foreach ($dataPeople as $key => $data): ?>
        <li>
          <a href="assinatura.php?slugPerson=<?= $key ?>" target="_blank">
            <?= $data['name'] ?>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </body>
</html>
