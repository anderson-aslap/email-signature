<?php

require_once 'init.php';

$slug = $_GET['slugPerson'];

$dataInfoPeople = getInfoBySlug($slug);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Assinatura E-mail</title>

    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:400,700"
    />

    <style>
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
      }

      body {
        font-family: 'Roboto', sans-serif;
      }

      #name {
        font-size: 20px;
        font-weight: bold;
      }

      #dpt {
        font-size: 15px;
      }

      #imgSindi {
        width: 270px;
      }

      .icon {
        width: 18px;
        height: 18px;
      }

      section {
        display: flex;
        align-items: center;
        gap: 4px;
        margin-bottom: 8px;
      }

      #section1 {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }

      #social-links {
        display: flex;
        align-items: center;
        justify-content: space-around;
      }

      .icon-social-linkedin {
        width: 75px;
        height: 55px;
      }

      .icon-social-insta {
        width: 75px;
        height: 40px;
        margin-left: 10px;
      }

      .icon-social-face {
        width: 100px;
        height: 55px;
      }
    </style>
  </head>
  <body>
    <table>
      <tr>
        <td>
          <img src="assets/lado_esquerdo.png" width="250px" height="230px" />
        </td>
        <td>
          <div style="margin-left: 15px; max-height: 230px;">
            <section id="section1">
              <span id="name"><?= $dataInfoPeople['name'] ?></span>
              <span id="dpt"><?= $dataInfoPeople['function'] ?></span>
            </section>

            <section>
              <img src="assets/icon-loclization.png" class="icon" />
              <span><?= $dataInfoPeople['address'] ?></span>
            </section>


            <?php if (!empty($dataInfoPeople['phone'])): ?>
              <section>
                <img src="assets/icon-fone.png" class="icon" />
                <span><?= $dataInfoPeople['phone'] ?></span>
              </section>
            <?php endif; ?>

            <?php if (!empty($dataInfoPeople['whatsapp_number'])): ?>
              <section>
                <img src="assets/zap.png" class="icon" />
                <span><?= $dataInfoPeople['whatsapp_number'] ?></span>
              </section>
            <?php endif; ?>

            <section>
              <img src="assets/icon-email.png" class="icon" />
              <span><?= $dataInfoPeople['address_mail'] ?></span>
            </section>

            <section>
              <img src="assets/icon-world.png" class="icon" />
              <span>www.sindicomis.com.br</span>
            </section>

            <section id="social-links">
              <a href="https://www.linkedin.com/company/sindicomis-actc" target="_blank">
                <img
                  src="assets/icon-linkedin.png"
                  class="icon-social-linkedin"
                />
              </a>

              <a href="https://www.instagram.com/sindicomis_actc" target="_blank">
                <img src="assets/icon-insta.png" class="icon-social-insta" />
              </a>

              <a href="https://www.facebook.com/sindicomiseactc" target="_blank">
                <img src="assets/icon-face.png" class="icon-social-face" />
              </a>
            </section>
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
