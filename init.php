<?php

include 'data.php';

function getPeople()
{
    global $data;
    return $data;
}

function getInfoBySlug($slugPerson)
{
    global $data;
    return $data[$slugPerson];
}
