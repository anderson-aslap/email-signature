<?php

$data = [
    '@deborah_kandratavicius' => [
        'name' => 'DEBORAH KANDRATAVÍCIUS',
        'function' => 'Assessora da Presidência',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599',
        'address_mail' => 'deborah.actc@sindicomis.com.br',
    ],

    '@edmilson_salorno' => [
        'name' => 'EDMILSOM SALORNO',
        'function' => 'Gerente Financeiro',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599',
        'address_mail' => 'edmilsom.actc@sindicomis.com.br',
    ],

    '@isabela_zorat_alonso' => [
        'name' => 'ISABELA ZORAT ALONSO',
        'function' => 'Assessora da Presidência',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599 - Ramal 1003',
        'address_mail' => 'isabela.actc@sindicomis.com.br',
    ],

    '@luiz_ramos' => [
        'name' => 'LUIZ RAMOS',
        'function' => 'Presidente',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'whatsapp_number' => '(19) 99111-8228',
        'phone' => '(11) 3255-2599',
        'address_mail' => 'presidente.actc@sindicomis.com.br',
    ],

    '@marcela_martins' => [
        'name' => 'MARCELA MARTINS',
        'function' =>
            'Assessora de Relações <br> Governamentais e Institucionais',
        'phone' => '',
        'address' => 'Brasília/DF',
        'whatsapp_number' => '(61) 99835-8123',
        'address_mail' => 'marcela.actc@sindicomis.com.br',
    ],

    '@oswaldo_castro' => [
        'name' => 'OSWALDO CASTRO',
        'function' => 'Assessor Técnico e Jurídico',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'whatsapp_number' => '(19) 99798-5013',
        'phone' => '(19) 3303-1163',
        'address_mail' => 'castro.actc@sindicomis.com.br',
    ],

    '@rebeca_dud_kapp' => [
        'name' => 'REBECA DUD KAPP',
        'function' => 'Consultora de Novos Negócios',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599',
        'address_mail' => 'rebeca.actc@sindicomis.com.br',
    ],

    '@regina_lima' => [
        'name' => 'REGINA LIMA',
        'function' => 'Coordenadora Administrativa',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599 / 3197-2972',
        'address_mail' => 'regina.actc@sindicomis.com.br',
    ],

    '@rene_braguin' => [
        'name' => 'RENÊ BRAGUIN',
        'function' => 'Supervisor Administrativo',
        'address' => 'Rua Avanhandava, 126 -6º Andar',
        'phone' => '(11) 3255-2599 / 3255-2310',
        'address_mail' => 'rene.actc@sindicomis.com.br',
    ],

    '@tatiane_moura' => [
        'name' => 'TATIANE MOURA',
        'function' =>
            'Assessora de Relações <br> Governamentais e Institucionais',
        'address' => 'Brasília/DF',
        'whatsapp_number' => '(61) 99165-7547',
        'address_mail' => 'tatianemary.actc@sindicomis.com.br',
    ],
];
